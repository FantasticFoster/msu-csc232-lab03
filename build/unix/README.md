# Unix Makefile generator

To generate Unix makefiles for this repo, type the following at a 
command-line prompt:

```
$ cmake -G "Unix Makefiles" ../..
```

It is assumed you are currently in the `build/unix` subdirectory of this repo.

To see what other generators may be available on your system, type:

```
$ cmake --help
```
